#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <pthread.h>
#include <sys/time.h>
#include <vector.h>
#include <matrix.h>

using namespace std;

#define num_thread 2

void dispMatrix(int rows, int coloumns, vector<int> v){
    cout<<endl<<"Matrix "<<rows<<" x "<<coloumns<<endl<<"   ---   "<<endl;
    for(int i = 0 ; i < rows ; i++){
        
	int j=0;
	while(j<coloumns){
            cout<<v[(i*coloumns)+j]<<" ";
		j++;        
	}
        cout << endl;
    }
}

N_Dim_Vector n(1500);
M_By_N_Matrix A(1500,1500);
M_By_N_Matrix B(1500,1500);
vector<int> An(1500);
vector<int> AB(2250000);



void dispMatrix(int rows, int coloumns, vector<int> v){
    cout<<endl<<"Matrix "<<rows<<" x "<<coloumns<<endl<<"-----------"<<endl;
    for(int i = 0 ; i < rows ; i++){
      
int j = 0;
while (j<coloumns){
            cout<<v[(i*coloumns)+j]<<" ";
	j++;        
	}
        cout << endl;
    }
}

N_Dim_Vector n(1500);
M_By_N_Matrix A(1500,1500);
M_By_N_Matrix B(1500,1500);
vector<int> An(1500);
vector<int> AB(2250000);

int main()
	{
	pthread_t* thread;
	struct timeval start, end;
	long mtime, seconds, useconds;    

	cout<<"With "<<num_thread<<" threads:-"<<endl;
	cout<<"Matrix Multiplication Vector Multiplication"<<endl;
	thread = (pthread_t*) malloc(num_thread*sizeof(pthread_t));
	gettimeofday(&start, NULL);
	for (int i = 0; i < num_thread; i++)
	{
	  
	  if (pthread_create (&thread[i], NULL, MatrixVectorMultiplication, (void*)i) != 0 )
	  {
	    perror("Can't create thread");
	    free(thread);
	    exit(-1);
	  }
	}

	
	for (int i = 0; i < num_thread; i++)
	  pthread_join (thread[i], NULL);
	gettimeofday(&end, NULL);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	cout<<"Elapsed time: "<<mtime<<endl;

	cout<<endl<<"Matrix Matrix Multiplication "<<endl;
	thread = (pthread_t*) malloc(num_thread*sizeof(pthread_t));
	gettimeofday(&start, NULL);
	
		int i =0;
	while(i< num_thread)
	{
	  
	  if (pthread_create (&thread[i], NULL, MatrixMatrixMultiplication, (void*)i) != 0 )
	  {
	    perror("Thread Can't be created");
	    free(thread);
	    exit(-1);
	  }
	i++;
	}

	
	for (int i = 0; i < num_thread; i++)
	  pthread_join (thread[i], NULL);
	gettimeofday(&end, NULL);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	cout<<"Elapsed time: "<<mtime<<endl;


	pthread_exit(NULL);
	getchar();
	return 0;
	}

