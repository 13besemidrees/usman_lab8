#include <vector>
#include <random>
#include <chrono>
#define num_thread2 2
using namespace std;

void* MatrixMatrixMultiplication(void* slice);
class M_By_N_Matrix{
    public:
    const int size;
    const int row;
    const int col;
    int j = 0;
    vector<int> *vec = new vector<int>();
    M_By_N_Matrix(int x, int y) : row(x), col(y), size(x*y){
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937 generator(seed);
	while (j<size)
	{	
		vec->push_back(generator() % 6);
		j++;
	}

        
};
