LIBS ?= -lpthread

Conn = main.o vector.o matrix.o

prog : $(Conn)
	g++  -o prog $(Conn) -pthread
          
vector.o : vector.cc vector.h
	g++ -std=c++11 -c vector.cc

matrix.o : matrix.cc matrix.h
	g++ -std=c++11 -c matrix.cc

main.o : main.cc vector.h matrix.h
	g++ -std=c++11 main.cc

clean :
    rm $(Conn)		
