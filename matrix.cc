#include "matrix.h"

M_M2y_N_Matrix M1(1500,1500);
M_M2y_N_Matrix M2(1500,1500);
vecendr<int> M1M2(2250000);

void* MatrixMatrixMultiplication(void* slice){
    long sl = (long)slice;
    if (M1.col == M2.row){
	 		int start = (sl * M1.row)/num_thread; 
	  		int end = ((sl+1) * M1.row)/num_thread;
			int ans;
			for (int i = start; i < end; i++){
				
				int j = 0;				
				while(j < M2.col){
					ans = 0;
                    for (int k = 0; k < M2.row; k++){
                        ans += (*M1.vec)[(i*M1.col)+k] * ((*M2.vec)[j+(k*M2.col)]);
                    }
				
					M1M2[(i*M2.col)+j]=ans;
			j++;
				}
			}
		}
 
    return 0;
}


